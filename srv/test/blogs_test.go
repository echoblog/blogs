package test

import (
	"context"
	"testing"
	"time"

	_ "github.com/go-sql-driver/mysql"

	"gitlab.com/echoblog/blogs/srv/models"

	"github.com/jinzhu/gorm"
	"github.com/micro/go-micro/client"
	blogconfigproto "gitlab.com/echoblog/blogs/srv/proto/blogconfig"
	blogsproto "gitlab.com/echoblog/blogs/srv/proto/blogs"
	categorysproto "gitlab.com/echoblog/blogs/srv/proto/categorys"
)

var (
	sw     = true
	DBPASS = ""
)

// database connector
func initDB() (*gorm.DB, error) {
	connStr := "root" + ":" + DBPASS + "@tcp(" + "127.0.0.1" +
		":" + "3306" + ")/" + "echoblog_blogs" + "?loc=Local&parseTime=true&charset=utf8mb4&collation=utf8mb4_unicode_ci"

	d, err := gorm.Open("mysql", connStr)
	if err != nil {
		return nil, err
	}
	d.DB().SetMaxIdleConns(10)
	d.DB().SetMaxOpenConns(100)
	d.DB().SetConnMaxLifetime(time.Second * 10)
	d.LogMode(true)
	return d, nil
}

func TestBlogsGRPC(t *testing.T) {
	if !sw {
		return
	}

	d, err := initDB()
	if err != nil {
		t.Fatal(err)
	}

	bClient := blogsproto.NewBlogsService("go.micro.srv.echoblog.blogs", client.DefaultClient)
	cClient := categorysproto.NewCategorysService("go.micro.srv.echoblog.blogs", client.DefaultClient)
	bcClient := blogconfigproto.NewBlogConfigService("go.micro.srv.echoblog.blogs", client.DefaultClient)

	cbRes, err := bClient.Create(context.Background(), &blogsproto.EchoblogBlogsCreateREQ{
		Blog: &blogsproto.Blog{
			Uid:         1,
			Domain:      "fortest",
			Title:       "TestBlog",
			Description: "Testing!",
		},
	})
	if err != nil {
		t.Fatal(err)
		return
	}
	if cbRes.Errcode != 0 || cbRes.Id <= 0 {
		t.Fatalf("%+v\n%+v\n", cbRes.Errcode, cbRes.Id)
		return
	}
	bID := cbRes.Id

	cbRes, err = bClient.Create(context.Background(), &blogsproto.EchoblogBlogsCreateREQ{
		Blog: &blogsproto.Blog{
			Uid:         1,
			Domain:      "fortest",
			Title:       "TestBlog",
			Description: "Testing!",
		},
	})
	if err != nil {
		t.Fatal(err)
		return
	}
	if cbRes.Errcode != 24 || cbRes.Id != 0 {
		t.Fatalf("%+v\n%+v\n", cbRes.Errcode, cbRes.Id)
		return
	}

	gbRes, err := bClient.Get(context.Background(), &blogsproto.EchoblogBlogsGetREQ{
		Id: bID,
	})
	if err != nil {
		t.Fatal(err)
		return
	}
	if gbRes.Errcode != 0 || gbRes.Result == nil {
		t.Fatalf("%+v\n%+v\n", gbRes.Errcode, gbRes.Result)
		return
	}

	gbRes = nil
	gbRes, err = bClient.Get(context.Background(), &blogsproto.EchoblogBlogsGetREQ{
		Domain: "fortest",
	})
	if err != nil {
		t.Fatal(err)
		return
	}
	if gbRes.Errcode != 0 || gbRes.Result == nil {
		t.Fatalf("%+v\n%+v\n", gbRes.Errcode, gbRes.Result)
		return
	}

	gbRes = nil
	gbRes, err = bClient.Get(context.Background(), &blogsproto.EchoblogBlogsGetREQ{
		Uid: 1,
	})
	if err != nil {
		t.Fatal(err)
		return
	}
	if gbRes.Errcode != 0 || gbRes.Result == nil {
		t.Fatalf("%+v\n%+v\n", gbRes.Errcode, gbRes.Result)
		return
	}

	bClient.Update(context.Background(), &blogsproto.EchoblogBlogsUpdateREQ{
		Blog: &blogsproto.Blog{
			Id:          bID,
			Uid:         1,
			Domain:      "fortest",
			Title:       "TestBlog[modified]",
			Description: "Testing![modified]",
		},
	})

	gbcRes, err := bcClient.Get(context.Background(), &blogconfigproto.EchoblogBlogConfigGetREQ{
		Bid: bID,
	})
	if err != nil {
		t.Fatal(err)
		return
	}
	if gbcRes.Errcode != 0 || gbcRes.Result == nil {
		t.Fatalf("%+v\n%+v\n", gbcRes.Errcode, gbcRes.Result)
		return
	}

	ubcRes, err := bcClient.Update(context.Background(), &blogconfigproto.EchoblogBlogConfigUpdateREQ{
		Blogconfig: &blogconfigproto.BlogConfig{
			Bid:               bID,
			Commentpermission: 2,
		},
	})
	if err != nil {
		t.Fatal(err)
		return
	}
	if ubcRes.Errcode != 0 {
		t.Fatalf("%+v\n", ubcRes.Errcode)
		return
	}

	ccRes, err := cClient.Create(context.Background(), &categorysproto.EchoblogCategoryCreateREQ{
		Category: &categorysproto.Category{
			Bid:   bID,
			Title: "testcategory",
			Index: 0,
		},
	})
	if err != nil {
		t.Fatal(err)
		return
	}
	if ccRes.Errcode != 0 || ccRes.Id == 0 {
		t.Fatalf("%+v\n%+v\n", ccRes.Errcode, ccRes.Id)
		return
	}
	cID := ccRes.Id

	lcRes, err := cClient.List(context.Background(), &categorysproto.EchoblogCategoryListREQ{
		Bid: bID,
	})
	if err != nil {
		t.Fatal(err)
		return
	}
	if lcRes.Errcode != 0 || lcRes.Result == nil {
		t.Fatalf("%+v\n%+v\n", lcRes.Errcode, lcRes.Result)
		return
	}

	gcRes, err := cClient.Get(context.Background(), &categorysproto.EchoblogCategoryGetREQ{
		Id:  cID,
		Bid: bID,
	})
	if err != nil {
		t.Fatal(err)
		return
	}
	if gcRes.Errcode != 0 || gcRes.Result == nil {
		t.Fatalf("%+v\n%+v\n", gcRes.Errcode, gcRes.Result)
		return
	}

	ucRes, err := cClient.Update(context.Background(), &categorysproto.EchoblogCategoryUpdateREQ{
		Category: &categorysproto.Category{
			Id:    cID,
			Bid:   bID,
			Title: "testcategory[modified]",
			Index: 1,
		},
	})
	if err != nil {
		t.Fatal(err)
		return
	}
	if ucRes.Errcode != 0 {
		t.Fatalf("%+v\n", ucRes.Errcode)
		return
	}

	dcRes, err := cClient.Delete(context.Background(), &categorysproto.EchoblogCategoryDeleteREQ{
		Id:  cID,
		Bid: bID,
	})
	if err != nil {
		t.Fatal(err)
		return
	}
	if dcRes.Errcode != 0 {
		t.Fatalf("%+v\n", dcRes.Errcode)
		return
	}

	cd := d.Model(&models.BlogConfig{}).Delete(&models.BlogConfig{
		BID: bID,
	})
	if cd.Error != nil {
		t.Fatal(err)
		return
	}
	cd = d.Model(&models.Blogs{}).Delete(&models.Blogs{
		ID: bID,
	})
	if cd.Error != nil {
		t.Fatal(err)
		return
	}
	return
}
