package handlers

import (
	"context"

	opentracing "github.com/opentracing/opentracing-go"
	"gitlab.com/echoblog/blogs/srv/models"
	"gitlab.com/echoblog/blogs/srv/proto/blogs"
	"gitlab.com/echoblog/utils"
)

// BlogsHandler handler
type BlogsHandler struct {
	M models.BlogsModel
}

// NewBlogsHandler registe new grpc handler
func NewBlogsHandler(m models.BlogsModel) *BlogsHandler {
	return &BlogsHandler{
		M: m,
	}
}

// Create ...
func (h *BlogsHandler) Create(ctx context.Context, in *blogs.EchoblogBlogsCreateREQ, out *blogs.EchoblogBlogsCreateRES) error {
	// tracking
	sp := utils.ExtractContext(ctx, opentracing.GlobalTracer(), "BlogsHandler.Create")
	defer sp.Finish()
	// logic
	defer func() {
		if out.Errcode != 0 && out.Id != 0 {
			// delete blog when init error
			h.M.DeleteBlog(sp.Context(), &models.Blogs{
				ID:     out.Id,
				Domain: in.Blog.Domain,
				UID:    in.Blog.Uid,
			})
		}
	}()
	out.Id = 0
	if in.Blog.Uid <= 0 || in.Blog.Domain == "" || in.Blog.Title == "" {
		out.Errcode = 1
		return nil
	}
	bobj, err := h.M.GetBlog(sp.Context(), &models.Blogs{
		Domain: in.Blog.Domain,
		UID:    in.Blog.Uid,
	})
	if err != nil {
		out.Errcode = 23
		return nil
	}
	if bobj != nil {
		out.Errcode = 24
		return nil
	}
	id, err := h.M.InsertBlog(sp.Context(), &models.Blogs{
		UID:         in.Blog.Uid,
		Domain:      in.Blog.Domain,
		Title:       in.Blog.Title,
		Description: in.Blog.Description,
	})
	if err != nil {
		out.Errcode = 25
		return nil
	}
	out.Errcode = 0
	out.Id = id
	// init blog config
	err = h.M.InsertBlogConfig(sp.Context(), &models.BlogConfig{
		BID:               id,
		CommentPermission: 1,
		BlogPermission:    1,
	})
	if err != nil {
		out.Errcode = 16
	}
	return nil
}

// Get ...
func (h *BlogsHandler) Get(ctx context.Context, in *blogs.EchoblogBlogsGetREQ, out *blogs.EchoblogBlogsGetRES) error {
	// tracking
	sp := utils.ExtractContext(ctx, opentracing.GlobalTracer(), "BlogsHandler.Get")
	defer sp.Finish()
	// logic
	out.Result = nil
	if in.Id <= 0 && in.Domain == "" && in.Uid <= 0 {
		out.Errcode = 1
		return nil
	}
	result, err := h.M.GetBlog(sp.Context(), &models.Blogs{
		ID:     in.Id,
		UID:    in.Uid,
		Domain: in.Domain,
	})
	if err != nil {
		out.Errcode = 23
		return nil
	}
	if result != nil {
		out.Result = &blogs.Blog{
			Id:          result.ID,
			Uid:         result.UID,
			Domain:      result.Domain,
			Title:       result.Title,
			Description: result.Description,
			Createdat:   result.CreatedAt.Unix(),
		}
	}
	out.Errcode = 0
	return nil
}

// Update ...
func (h *BlogsHandler) Update(ctx context.Context, in *blogs.EchoblogBlogsUpdateREQ, out *blogs.EchoblogBlogsUpdateRES) error {
	// tracking
	sp := utils.ExtractContext(ctx, opentracing.GlobalTracer(), "BlogsHandler.Update")
	defer sp.Finish()
	// logic
	if in.Blog.Id <= 0 || in.Blog.Domain == "" || in.Blog.Uid <= 0 {
		out.Errcode = 1
		return nil
	}
	err := h.M.UpdateBlog(sp.Context(), &models.Blogs{
		ID:          in.Blog.Id,
		UID:         in.Blog.Uid,
		Domain:      in.Blog.Domain,
		Title:       in.Blog.Title,
		Description: in.Blog.Description,
	})
	if err != nil {
		out.Errcode = 26
		return nil
	}
	out.Errcode = 0
	return nil
}
