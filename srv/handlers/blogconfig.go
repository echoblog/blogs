package handlers

import (
	"context"

	opentracing "github.com/opentracing/opentracing-go"
	"gitlab.com/echoblog/blogs/srv/models"
	"gitlab.com/echoblog/blogs/srv/proto/blogconfig"
	"gitlab.com/echoblog/utils"
)

// BlogConfigHandler handler
type BlogConfigHandler struct {
	M models.BlogsModel
}

// NewBlogConfigHandler registe new grpc handler
func NewBlogConfigHandler(m models.BlogsModel) *BlogConfigHandler {
	return &BlogConfigHandler{
		M: m,
	}
}

// // Create ...
// func (h *BlogConfigHandler) Create(ctx context.Context, in *blogconfig.EchoblogBlogConfigCreateREQ, out *blogconfig.EchoblogBlogConfigCreateRES) error {
// 	// tracking
// 	sp := utils.ExtractContext(ctx, opentracing.GlobalTracer(), "BlogConfigHandler.Create")
// 	defer sp.Finish()
// 	// logic
// 	if in.Blogconfig.Bid <= 0 {
// 		out.Errcode = 1
// 		return nil
// 	}
// 	bcobj, err := h.M.GetBlogConfig(sp.Context(), &models.BlogConfig{BID: in.Blogconfig.Bid})
// 	if err != nil {
// 		out.Errcode = 15
// 		return nil
// 	}
// 	if bcobj != nil {
// 		out.Errcode = 0
// 		return nil
// 	}
// 	err = h.M.InsertBlogConfig(sp.Context(), &models.BlogConfig{
// 		BID:               in.Blogconfig.Bid,
// 		CommentPermission: uint(in.Blogconfig.Commentpermission),
// 		BlogPermission:    uint(in.Blogconfig.Blogpermission),
// 	})
// 	if err != nil {
// 		out.Errcode = 16
// 		return nil
// 	}
// 	out.Errcode = 0
// 	return nil
// }

// Get ...
func (h *BlogConfigHandler) Get(ctx context.Context, in *blogconfig.EchoblogBlogConfigGetREQ, out *blogconfig.EchoblogBlogConfigGetRES) error {
	// tracking
	sp := utils.ExtractContext(ctx, opentracing.GlobalTracer(), "BlogConfigHandler.Get")
	defer sp.Finish()
	// logic
	if in.Bid <= 0 {
		out.Errcode = 1
		return nil
	}
	bcobj, err := h.M.GetBlogConfig(sp.Context(), &models.BlogConfig{BID: in.Bid})
	if err != nil {
		out.Errcode = 15
		return nil
	}
	if bcobj != nil {
		out.Result = &blogconfig.BlogConfig{
			Bid:               bcobj.BID,
			Commentpermission: uint64(bcobj.CommentPermission),
			Blogpermission:    uint64(bcobj.BlogPermission),
		}
	}
	return nil
}

// Update ...
func (h *BlogConfigHandler) Update(ctx context.Context, in *blogconfig.EchoblogBlogConfigUpdateREQ, out *blogconfig.EchoblogBlogConfigUpdateRES) error {
	// tracking
	sp := utils.ExtractContext(ctx, opentracing.GlobalTracer(), "BlogConfigHandler.Update")
	defer sp.Finish()
	// logic
	if in.Blogconfig.Bid <= 0 {
		out.Errcode = 1
		return nil
	}
	err := h.M.UpdateBlogConfig(sp.Context(), &models.BlogConfig{
		BID:               in.Blogconfig.Bid,
		CommentPermission: uint(in.Blogconfig.Commentpermission),
		BlogPermission:    uint(in.Blogconfig.Blogpermission),
	})
	if err != nil {
		out.Errcode = 17
		return nil
	}
	out.Errcode = 0
	return nil
}
