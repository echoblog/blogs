package handlers

import (
	"context"

	opentracing "github.com/opentracing/opentracing-go"
	"gitlab.com/echoblog/blogs/srv/models"
	"gitlab.com/echoblog/blogs/srv/proto/categorys"
	"gitlab.com/echoblog/utils"
)

// CategorysHandler handler
type CategorysHandler struct {
	M models.BlogsModel
}

// NewCategorysHandler registe new grpc handler
func NewCategorysHandler(m models.BlogsModel) *CategorysHandler {
	return &CategorysHandler{
		M: m,
	}
}

// List ..
func (h *CategorysHandler) List(ctx context.Context, in *categorys.EchoblogCategoryListREQ, out *categorys.EchoblogCategoryListRES) error {
	// tracking
	sp := utils.ExtractContext(ctx, opentracing.GlobalTracer(), "CategorysHandler.List")
	defer sp.Finish()
	// logic
	out.Result = nil
	if in.Bid <= 0 {
		out.Errcode = 1
		return nil
	}
	clist, err := h.M.SelectCategory(sp.Context(), &models.Categorys{BID: in.Bid})
	if err != nil {
		out.Errcode = 18
		return nil
	}
	var result []*categorys.Category
	for _, cobj := range clist {
		result = append(result, &categorys.Category{
			Bid:   cobj.ID,
			Index: uint64(cobj.Index),
			Title: cobj.Title,
		})
	}
	out.Result = result
	out.Errcode = 0
	return nil
}

// Get ..
func (h *CategorysHandler) Get(ctx context.Context, in *categorys.EchoblogCategoryGetREQ, out *categorys.EchoblogCategoryGetRES) error {
	// tracking
	sp := utils.ExtractContext(ctx, opentracing.GlobalTracer(), "CategorysHandler.Get")
	defer sp.Finish()
	// logic
	out.Result = nil
	if in.Id <= 0 || in.Bid <= 0 {
		out.Errcode = 1
		return nil
	}
	cobj, err := h.M.GetCategory(sp.Context(), &models.Categorys{BID: in.Bid, ID: in.Id})
	if err != nil {
		out.Errcode = 19
		return nil
	}
	if cobj != nil {
		out.Result = &categorys.Category{
			Id:    cobj.ID,
			Bid:   cobj.BID,
			Index: uint64(cobj.Index),
			Title: cobj.Title,
		}
	}
	out.Errcode = 0
	return nil
}

// Create ..
func (h *CategorysHandler) Create(ctx context.Context, in *categorys.EchoblogCategoryCreateREQ, out *categorys.EchoblogCategoryCreateRES) error {
	// tracking
	sp := utils.ExtractContext(ctx, opentracing.GlobalTracer(), "CategorysHandler.Create")
	defer sp.Finish()
	// logic
	out.Id = 0
	if in.Category.Title == "" || in.Category.Bid <= 0 {
		out.Errcode = 1
		return nil
	}
	id, err := h.M.InsertCategory(sp.Context(), &models.Categorys{
		BID:   in.Category.Bid,
		Title: in.Category.Title,
		Index: uint(in.Category.Index),
	})
	if err != nil {
		out.Errcode = 20
		return nil
	}
	out.Id = id
	out.Errcode = 0
	return nil
}

// Update ..
func (h *CategorysHandler) Update(ctx context.Context, in *categorys.EchoblogCategoryUpdateREQ, out *categorys.EchoblogCategoryUpdateRES) error {
	// tracking
	sp := utils.ExtractContext(ctx, opentracing.GlobalTracer(), "CategorysHandler.Update")
	defer sp.Finish()
	// logic
	if in.Category.Id <= 0 || in.Category.Bid <= 0 || in.Category.Title == "" {
		out.Errcode = 1
		return nil
	}
	err := h.M.UpdateCategory(sp.Context(), &models.Categorys{
		ID:    in.Category.Id,
		BID:   in.Category.Bid,
		Title: in.Category.Title,
		Index: uint(in.Category.Index),
	})
	if err != nil {
		out.Errcode = 21
		return nil
	}
	out.Errcode = 0
	return nil
}

// Delete ..
func (h *CategorysHandler) Delete(ctx context.Context, in *categorys.EchoblogCategoryDeleteREQ, out *categorys.EchoblogCategoryDeleteRES) error {
	// tracking
	sp := utils.ExtractContext(ctx, opentracing.GlobalTracer(), "CategorysHandler.Delete")
	defer sp.Finish()
	// logic
	if in.Id <= 0 || in.Bid <= 0 {
		out.Errcode = 1
		return nil
	}
	err := h.M.DeleteCategory(sp.Context(), &models.Categorys{
		ID:  in.Id,
		BID: in.Bid,
	})
	if err != nil {
		out.Errcode = 22
		return nil
	}
	out.Errcode = 0
	return nil
}
