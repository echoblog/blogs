package models

import (
	"github.com/jinzhu/gorm"
	opentracing "github.com/opentracing/opentracing-go"
	log "github.com/sirupsen/logrus"
)

// Categorys blog's cateogrys for article
type Categorys struct {
	ID    uint64 `gorm:"PRIMARY_KEY;AUTO_INCREMENT"`
	BID   uint64 `gorm:"index;type:int(16);NOT NULL"`
	Index uint   `gorm:"type:int(16);NOT NULL;default:0"`
	Title string `gorm:"type:varchar(32);NOT NULL"`
}

// SelectCategory ...
func (h *ModelHandler) SelectCategory(ctx opentracing.SpanContext, c *Categorys) (categorys []*Categorys, err error) {
	ts := opentracing.GlobalTracer().StartSpan("Models.SelectCategory", opentracing.ChildOf(ctx))
	defer ts.Finish()
	categorys = []*Categorys{}
	d := h.db.Where(&Categorys{BID: c.BID}).Order("index").Order("id DESC").Find(&categorys)
	err = d.Error
	if err == gorm.ErrRecordNotFound {
		return nil, nil
	}
	if err != nil {
		log.WithError(err).Errorln("blogs.categorys.SelectCategory error[0]")
		return nil, err
	}
	return categorys, nil
}

// GetCategory ...
func (h *ModelHandler) GetCategory(ctx opentracing.SpanContext, c *Categorys) (category *Categorys, err error) {
	ts := opentracing.GlobalTracer().StartSpan("Models.GetCategory", opentracing.ChildOf(ctx))
	defer ts.Finish()
	category = &Categorys{}
	d := h.db.Where(&Categorys{ID: c.ID, BID: c.BID}).First(category)
	err = d.Error
	if err == gorm.ErrRecordNotFound {
		return nil, nil
	}
	if err != nil {
		log.WithError(err).Errorln("blogs.categorys.GetCategory error[0]")
		return nil, err
	}
	return category, nil
}

// InsertCategory ...
func (h *ModelHandler) InsertCategory(ctx opentracing.SpanContext, c *Categorys) (id uint64, err error) {
	ts := opentracing.GlobalTracer().StartSpan("Models.InsertCategory", opentracing.ChildOf(ctx))
	defer ts.Finish()
	d := h.db.Create(c)
	if d.Error != nil {
		log.WithError(err).Errorln("blogs.categorys.InsertCategory error[0]")
		return 0, d.Error
	}
	return c.ID, nil
}

// UpdateCategory ...
func (h *ModelHandler) UpdateCategory(ctx opentracing.SpanContext, c *Categorys) (err error) {
	ts := opentracing.GlobalTracer().StartSpan("Models.UpdateCategory", opentracing.ChildOf(ctx))
	defer ts.Finish()
	d := h.db.Model(&Categorys{}).Where(&Categorys{ID: c.ID, BID: c.BID}).Updates(&Categorys{
		Title: c.Title,
		Index: c.Index,
	})
	if d.Error != nil {
		log.WithError(err).Errorln("blogs.categorys.UpdateCategory error[0]")
		return d.Error
	}
	return nil
}

// DeleteCategory ...
func (h *ModelHandler) DeleteCategory(ctx opentracing.SpanContext, c *Categorys) (err error) {
	ts := opentracing.GlobalTracer().StartSpan("Models.DeleteCategory", opentracing.ChildOf(ctx))
	defer ts.Finish()
	d := h.db.Delete(c)
	if d.Error != nil {
		log.WithError(err).Errorln("blogs.categorys.DeleteCategory error[0]")
		return d.Error
	}
	return nil
}
