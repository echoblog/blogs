package models

import (
	"github.com/jinzhu/gorm"
	opentracing "github.com/opentracing/opentracing-go"
	log "github.com/sirupsen/logrus"
)

// BlogConfig config for blog
type BlogConfig struct {
	BID               uint64 `gorm:"PRIMARY_KEY;unique_index;type:int(16);NOT NULL"`
	CommentPermission uint   `gorm:"type:int(16);NOT NULL;default:0"`
	BlogPermission    uint   `gorm:"type:int(16);NOT NULL;default:0"`
}

// InsertBlogConfig ...
func (h *ModelHandler) InsertBlogConfig(ctx opentracing.SpanContext, bc *BlogConfig) (err error) {
	ts := opentracing.GlobalTracer().StartSpan("Models.InsertBlogConfig", opentracing.ChildOf(ctx))
	defer ts.Finish()
	d := h.db.Create(bc)
	if d.Error != nil {
		log.WithError(err).Errorln("blogs.blogconfig.InsertBlogConfig error[0]")
		return err
	}
	return nil
}

// GetBlogConfig ...
func (h *ModelHandler) GetBlogConfig(ctx opentracing.SpanContext, bc *BlogConfig) (blogconfig *BlogConfig, err error) {
	ts := opentracing.GlobalTracer().StartSpan("Models.GetBlogConfig", opentracing.ChildOf(ctx))
	defer ts.Finish()
	blogconfig = &BlogConfig{}
	d := h.db.Where(&BlogConfig{BID: bc.BID}).First(blogconfig)
	err = d.Error
	if err == gorm.ErrRecordNotFound {
		return nil, nil
	}
	if err != nil {
		log.WithError(err).Errorln("blogs.blogconfig.GetBlogConfig error[0]")
		return nil, err
	}
	return blogconfig, nil
}

// UpdateBlogConfig ...
func (h *ModelHandler) UpdateBlogConfig(ctx opentracing.SpanContext, bc *BlogConfig) (err error) {
	ts := opentracing.GlobalTracer().StartSpan("Models.UpdateBlogConfig", opentracing.ChildOf(ctx))
	defer ts.Finish()
	d := h.db.Model(&BlogConfig{}).Where(&BlogConfig{BID: bc.BID}).Updates(&BlogConfig{
		BlogPermission:    bc.BlogPermission,
		CommentPermission: bc.CommentPermission,
	})
	if d.Error != nil {
		log.WithError(err).Errorln("blogs.blogconfig.UpdateBlogConfig error[0]")
		return err
	}
	return nil
}
