package models

import (
	"github.com/jinzhu/gorm"
	"github.com/opentracing/opentracing-go"
)

// BlogsModel data operation interface for blogs
type BlogsModel interface {
	// blogs
	InsertBlog(ctx opentracing.SpanContext, b *Blogs) (id uint64, err error)
	GetBlog(ctx opentracing.SpanContext, b *Blogs) (blog *Blogs, err error)
	UpdateBlog(ctx opentracing.SpanContext, b *Blogs) (err error)
	DeleteBlog(ctx opentracing.SpanContext, b *Blogs) (err error)
	// blogconfig
	InsertBlogConfig(ctx opentracing.SpanContext, bc *BlogConfig) (err error)
	GetBlogConfig(ctx opentracing.SpanContext, bc *BlogConfig) (blogconfig *BlogConfig, err error)
	UpdateBlogConfig(ctx opentracing.SpanContext, bc *BlogConfig) (err error)
	// categorys
	SelectCategory(ctx opentracing.SpanContext, c *Categorys) (categorys []*Categorys, err error)
	GetCategory(ctx opentracing.SpanContext, c *Categorys) (category *Categorys, err error)
	InsertCategory(ctx opentracing.SpanContext, c *Categorys) (id uint64, err error)
	UpdateCategory(ctx opentracing.SpanContext, c *Categorys) (err error)
	DeleteCategory(ctx opentracing.SpanContext, c *Categorys) (err error)
}

// New model
func New(db *gorm.DB) BlogsModel {
	h := &ModelHandler{
		db: db,
	}

	// auto migrate tables
	h.db.AutoMigrate(&Blogs{})
	h.db.AutoMigrate(&Categorys{})
	h.db.AutoMigrate(&BlogConfig{})

	return h
}

// ModelHandler for data operation handler
type ModelHandler struct {
	db *gorm.DB
}
