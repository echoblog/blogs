package models

import (
	"time"

	"github.com/jinzhu/gorm"

	opentracing "github.com/opentracing/opentracing-go"
	log "github.com/sirupsen/logrus"
)

// Blogs blogs
type Blogs struct {
	ID          uint64 `gorm:"PRIMARY_KEY;AUTO_INCREMENT"`
	UID         uint64 `gorm:"unique_index;type:int(16);NOT NULL"`
	Domain      string `gorm:"unique_index;type:varchar(16);NOT NULL"`
	Title       string `gorm:"type:varchar(72);NOT NULL"`
	Description string `gorm:"type:text;default:''"`
	CreatedAt   time.Time
}

// InsertBlog ...
func (h *ModelHandler) InsertBlog(ctx opentracing.SpanContext, b *Blogs) (id uint64, err error) {
	ts := opentracing.GlobalTracer().StartSpan("Models.InsertBlog", opentracing.ChildOf(ctx))
	defer ts.Finish()
	b.CreatedAt = time.Now()
	d := h.db.Create(b)
	if d.Error != nil {
		log.WithError(err).Errorln("blogs.blogs.InsertBlog error[0]")
		return 0, d.Error
	}
	return b.ID, nil
}

// GetBlog ...
func (h *ModelHandler) GetBlog(ctx opentracing.SpanContext, b *Blogs) (blog *Blogs, err error) {
	ts := opentracing.GlobalTracer().StartSpan("Models.GetBlog", opentracing.ChildOf(ctx))
	defer ts.Finish()
	blog = &Blogs{}
	d := h.db.Where(&Blogs{ID: b.ID}).Or(&Blogs{Domain: b.Domain}).Or(&Blogs{UID: b.UID}).First(blog)
	err = d.Error
	if err == gorm.ErrRecordNotFound {
		return nil, nil
	}
	if err != nil {
		log.WithError(err).Errorln("blogs.blogs.GetBlog error[0]")
		return nil, err
	}
	return blog, nil
}

// UpdateBlog ...
func (h *ModelHandler) UpdateBlog(ctx opentracing.SpanContext, b *Blogs) (err error) {
	ts := opentracing.GlobalTracer().StartSpan("Models.UpdateBlog", opentracing.ChildOf(ctx))
	defer ts.Finish()
	d := h.db.Model(&Blogs{}).Where(&Blogs{ID: b.ID, UID: b.UID, Domain: b.Domain}).Updates(&Blogs{
		Title:       b.Title,
		Description: b.Description,
	})
	if d.Error != nil {
		log.WithError(err).Errorln("blogs.blogs.UpdateBlog error[0]")
		return d.Error
	}
	return nil
}

// DeleteBlog ...
func (h *ModelHandler) DeleteBlog(ctx opentracing.SpanContext, b *Blogs) (err error) {
	ts := opentracing.GlobalTracer().StartSpan("Models.DeleteBlog", opentracing.ChildOf(ctx))
	defer ts.Finish()
	d := h.db.Delete(b)
	if d.Error != nil {
		log.WithError(err).Errorln("blogs.blogs.DeleteBlog error[0]")
		return d.Error
	}
	return nil
}
